/*
 * Exemplo de Programação Multicast
 *
 * Autor: Lucio Agostinho Rocha
 * Ultima atualizacao: 20/10/2017
 *
 * (Baseado em: http://docs.oracle.com/javase/tutorial/networking/datagrams/examples/MulticastClient.java)
 *
*/
package multicast;

public class MulticastServer {
    public static void main(String[] args) throws java.io.IOException {
        new MulticastServerThread().start();
    }
}
