/*
 * Exemplo de Programação Multicast
 *
 * Autor: Lucio Agostinho Rocha
 * Ultima atualizacao: 20/10/2017
 *
 * (Baseado em: http://docs.oracle.com/javase/tutorial/networking/datagrams/examples/MulticastClient.java)
 *
Exercicio:
1) Crie o seu próprio arquivo de notícias com 10 noticias suas, com o nome: 'noticias.txt'
2) Modifique o código para que exista apenas uma classe servidor de nome MulticastServer.
3) Informe uma mensagem quando o servidor multicast iniciar.
4) Informe o tempo de dormência de cada iteração do servidor.
5) Informe um novo endereço multicast para o socket cliente: 239.0.0.1:4447
6) Crie uma classe 'Principal' que inicialize 10 threads clientes em um pool 
   de threads. Cada cliente deve
   exibir o seu indice antes da mensagem de cada noticia lida. 
7) Insira o codigo de finalizacao adequada do pool de threads clientes.
8) Faça com que o servidor não termine após iterar sobre as notícias, ou seja,
   após iterar até a última notícia, o servidor deve ser capaz de servir as
   mesmas notícias novamente. Dica: basta abrir o arquivo de noticias novamente.
 */
package multicast;

import java.io.*;
import java.net.*;
import java.util.*;
 
public class MulticastClient {
 
    public static void main(String[] args) throws IOException {
 
        MulticastSocket socket = new MulticastSocket(4447);
        InetAddress address = InetAddress.getByName("230.0.0.1");
    socket.joinGroup(address);
 
        DatagramPacket packet;
     
            // get a few quotes
    for (int i = 0; i < 5; i++) {
 
        byte[] buf = new byte[256];
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);
 
            String received = new String(packet.getData(), 0, packet.getLength());
            System.out.println("Quote of the Moment: " + received);
    }
 
    socket.leaveGroup(address);
    socket.close();
    }
 
}